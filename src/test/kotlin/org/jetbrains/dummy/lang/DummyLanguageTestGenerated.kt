package org.jetbrains.dummy.lang

import org.junit.Test

class DummyLanguageTestGenerated : AbstractDummyLanguageTest() {
    @Test
    fun testBlockConstantIfCondition() {
        doTest("testData/block/constantIfCondition.dummy")
    }

    @Test
    fun testBlockEmptyFunction() {
        doTest("testData/block/emptyFunction.dummy")
    }

    @Test
    fun testBlockEmptyIf() {
        doTest("testData/block/emptyIf.dummy")
    }

    @Test
    fun testFunctionAssignmentByVoidFunction() {
        doTest("testData/function/assignmentByVoidFunction.dummy")
    }

    @Test
    fun testFunctionAssignmentToItself() {
        doTest("testData/function/assignmentToItself.dummy")
    }

    @Test
    fun testFunctionCallNonExisting() {
        doTest("testData/function/callNonExisting.dummy")
    }

    @Test
    fun testFunctionDuplicatedDeclaration() {
        doTest("testData/function/duplicatedDeclaration.dummy")
    }

    @Test
    fun testFunctionIncompatibleReturnType() {
        doTest("testData/function/incompatibleReturnType.dummy")
    }

    @Test
    fun testFunctionIncompleteReturnCases() {
        doTest("testData/function/incompleteReturnCases.dummy")
    }

    @Test
    fun testFunctionUnreachableCode() {
        doTest("testData/function/unreachableCode.dummy")
    }

    @Test
    fun testGoodCode() {
        doTest("testData/goodCode.dummy")
    }

    @Test
    fun testVariableAccessBeforeInitialization() {
        doTest("testData/variable/accessBeforeInitialization.dummy")
    }

    @Test
    fun testVariableAccessNonExisting() {
        doTest("testData/variable/accessNonExisting.dummy")
    }

    @Test
    fun testVariableDuplicatedFunctionParameters() {
        doTest("testData/variable/duplicatedFunctionParameters.dummy")
    }

    @Test
    fun testVariableNameOverlapping() {
        doTest("testData/variable/nameOverlapping.dummy")
    }
}
