package org.jetbrains.dummy.lang.tree.visitor.function

import org.jetbrains.dummy.lang.checker.FunctionChecker
import org.jetbrains.dummy.lang.tree.Element
import org.jetbrains.dummy.lang.tree.File
import org.jetbrains.dummy.lang.tree.visitor.DummyLangVisitor

/**
 * Collects declaration information about functions.
 * Data : Map<FunName, Map<ParametersCount, DeclarationInfo>>
 */
class FunctionDeclarationVisitor(val checker: FunctionChecker) :
    DummyLangVisitor<Unit, MutableMap<String, MutableMap<Int, DeclaredFunction>>>() {

    override fun visitElement(element: Element, data: MutableMap<String, MutableMap<Int, DeclaredFunction>>) {
        element.acceptChildren(this, data)
    }

    override fun visitFile(file: File, data: MutableMap<String, MutableMap<Int, DeclaredFunction>>) {
        file.functions.forEach { function ->
            val funName = function.name
            val paramsCount = function.parameters.size

            val declaredFunctions = data.getOrPut(funName) { mutableMapOf() }
            val declaredFunction = declaredFunctions[paramsCount]

            // Checks if function with specific name and amount of parameters already declared
            if (declaredFunction != null) {
                checker.reportDuplicatedDeclaration(function, paramsCount, declaredFunction.line)
            } else {
                declaredFunctions[paramsCount] = DeclaredFunction(function.line, null)
            }
        }

        super.visitFile(file, data)
    }
}

/**
 * Represents information about function declaration.
 * @param line  Line number with declaration.
 * @param returnData  Information about return type.
 */
data class DeclaredFunction(val line: Int, var returnData: ReturnData?) {
    fun isVoid(): Boolean = returnData?.isVoid ?: true
}

/**
 * Represents information about function return type.
 * @param isVoid  If function return nothing.
 * @param inAllCases  If returns in all previous possible cases.
 */
data class ReturnData(val isVoid: Boolean, val inAllCases: Boolean)

