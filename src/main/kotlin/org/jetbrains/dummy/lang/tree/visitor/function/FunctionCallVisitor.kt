package org.jetbrains.dummy.lang.tree.visitor.function

import org.jetbrains.dummy.lang.checker.FunctionChecker
import org.jetbrains.dummy.lang.tree.*
import org.jetbrains.dummy.lang.tree.visitor.DummyLangVisitor

/**
 * Visitor for finding illegal function invocations.
 * Data : Map<FunName, Map<ParametersCount, DeclarationInfo>>
 */
class FunctionCallVisitor(val checker: FunctionChecker) :
    DummyLangVisitor<Unit, MutableMap<String, MutableMap<Int, DeclaredFunction>>>() {

    override fun visitElement(element: Element, data: MutableMap<String, MutableMap<Int, DeclaredFunction>>) {
        element.acceptChildren(this, data)
    }

    override fun visitFunctionCall(
        functionCall: FunctionCall,
        data: MutableMap<String, MutableMap<Int, DeclaredFunction>>
    ) {
        val funName = functionCall.function
        val paramsCount = functionCall.arguments.size

        // Report if invoked function does not exists
        val declaredFunctions = data[funName]
        if (declaredFunctions == null) {
            checker.reportCallNonExistingFunction(functionCall)
        } else if (!declaredFunctions.containsKey(paramsCount)) {
            checker.reportCallWithDifferentParametersCount(functionCall, paramsCount)
        }

        super.visitFunctionCall(functionCall, data)
    }

    override fun visitVariableDeclaration(
        variableDeclaration: VariableDeclaration,
        data: MutableMap<String, MutableMap<Int, DeclaredFunction>>
    ) {
        // Checks if initializer is void function invocation
        inspectAssignmentByVoidFunction(
            variableDeclaration,
            variableDeclaration.name,
            variableDeclaration.initializer,
            data
        )
        super.visitVariableDeclaration(variableDeclaration, data)
    }

    override fun visitAssignment(assignment: Assignment, data: MutableMap<String, MutableMap<Int, DeclaredFunction>>) {
        // Checks if rhs is invocation of void function
        inspectAssignmentByVoidFunction(assignment, assignment.variable, assignment.rhs, data)
        super.visitAssignment(assignment, data)
    }

    private fun inspectAssignmentByVoidFunction(
        statement: Statement,
        varName: String,
        call: Statement?,
        data: MutableMap<String, MutableMap<Int, DeclaredFunction>>
    ) {
        if (call is FunctionCall) {
            val declaredFun = data[call.function]?.get(call.arguments.size)
            if (declaredFun?.isVoid() == true) {
                checker.reportAssignmentByVoidFunction(statement, varName, call, declaredFun)
            }
        }
    }
}

