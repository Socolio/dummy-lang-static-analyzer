package org.jetbrains.dummy.lang.tree.visitor.function

import org.jetbrains.dummy.lang.checker.FunctionChecker
import org.jetbrains.dummy.lang.tree.*
import org.jetbrains.dummy.lang.tree.visitor.DummyLangVisitor

/**
 * Visitor for collecting information about functions return types.
 * Also checks for illegal return cases.
 * Data : Map<FunName, Map<ParametersCount, DeclarationInfo>>
 */
class FunctionInterruptionVisitor(val checker: FunctionChecker) :
    DummyLangVisitor<Unit, MutableMap<String, MutableMap<Int, DeclaredFunction>>>() {

    lateinit var currentFunction: DeclaredFunction

    override fun visitElement(element: Element, data: MutableMap<String, MutableMap<Int, DeclaredFunction>>) {
        element.acceptChildren(this, data)
    }

    override fun visitFunctionDeclaration(
        functionDeclaration: FunctionDeclaration,
        data: MutableMap<String, MutableMap<Int, DeclaredFunction>>
    ) {
        currentFunction = data[functionDeclaration.name]!![functionDeclaration.parameters.size]!!
        super.visitFunctionDeclaration(functionDeclaration, data)

        // Checks for last return statement for non void functions (if needed)
        val returnData = currentFunction.returnData
        if (returnData != null && !returnData.isVoid && !returnData.inAllCases) {
            checker.reportIncompleteReturnCases(functionDeclaration)
        }
    }

    override fun visitBlock(block: Block, data: MutableMap<String, MutableMap<Int, DeclaredFunction>>) {
        // Find first line after returning in all cases
        val unreachableLine = block.statements.find {
            val unreachable = currentFunction.returnData?.inAllCases == true
            if (!unreachable) {
                it.accept(this, data)
            }
            unreachable
        }

        if (unreachableLine != null) {
            checker.reportUnreachableCode(unreachableLine, block.statements.last())
        }
    }

    override fun visitIfStatement(
        ifStatement: IfStatement,
        data: MutableMap<String, MutableMap<Int, DeclaredFunction>>
    ) {
        // Backup data before updating it in if statement
        val oldReturnData = currentFunction.returnData

        // Visit 'then' block for finding return statements
        ifStatement.thenBlock.accept(this, data)
        val thenReturnData = currentFunction.returnData

        // Visit 'else' block if it exists
        var elseReturnData: ReturnData? = null
        if (ifStatement.elseBlock != null) {
            currentFunction.returnData = oldReturnData
            ifStatement.elseBlock.accept(this, data)
            elseReturnData = currentFunction.returnData

            // Checks for incompatible return types (void && !void) in 'then' and 'else' blocks
            if (thenReturnData != null && elseReturnData != null && thenReturnData.isVoid != elseReturnData.isVoid) {
                checker.reportIncompatibleCasesReturnType(ifStatement)
            }
        }

        // Merge new data
        currentFunction.returnData = when {
            thenReturnData != null && elseReturnData != null -> {
                ReturnData(thenReturnData.isVoid, thenReturnData.inAllCases && elseReturnData.inAllCases)
            }
            thenReturnData != null -> ReturnData(thenReturnData.isVoid, false)
            elseReturnData != null -> ReturnData(elseReturnData.isVoid, false)
            else -> oldReturnData
        }
    }

    override fun visitReturnStatement(
        returnStatement: ReturnStatement,
        data: MutableMap<String, MutableMap<Int, DeclaredFunction>>
    ) {
        val prevReturnData = currentFunction.returnData
        val returnData = ReturnData(returnStatement.result == null, true)

        // Checks for incompatible return type (void && !void) with previous return data
        if (prevReturnData != null && prevReturnData.isVoid != returnData.isVoid) {
            checker.reportIncompatibleWithPreviousReturnType(returnStatement)
        } else {
            currentFunction.returnData = returnData
        }
    }
}

