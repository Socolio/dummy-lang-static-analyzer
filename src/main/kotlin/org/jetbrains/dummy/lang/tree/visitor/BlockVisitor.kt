package org.jetbrains.dummy.lang.tree.visitor

import org.jetbrains.dummy.lang.checker.BlockChecker
import org.jetbrains.dummy.lang.tree.*

/**
 * Visitor for finding useless or empty blocks of code.
 */
class BlockVisitor(val checker: BlockChecker) : DummyLangVisitor<Unit, Unit>() {

    override fun visitElement(element: Element, data: Unit) {
        element.acceptChildren(this, data)
    }

    override fun visitFunctionDeclaration(functionDeclaration: FunctionDeclaration, data: Unit) {
        fun isEmptyReturn(statement: Statement) = statement is ReturnStatement && statement.result == null

        // Checks if function body is empty or contains single void return statement
        if (functionDeclaration.body.statements.isEmpty() || isEmptyReturn(functionDeclaration.body.statements.first())) {
            checker.reportEmptyFunction(functionDeclaration)
        }

        super.visitFunctionDeclaration(functionDeclaration, data)
    }

    override fun visitIfStatement(ifStatement: IfStatement, data: Unit) {
        // Checks for constant boolean condition
        if (ifStatement.condition is BooleanConst) {
            checker.reportConstantIfCondition(ifStatement, ifStatement.condition)
        }

        val emptyThen = ifStatement.thenBlock.statements.isEmpty()
        val emptyElse = ifStatement.elseBlock?.statements?.isEmpty() ?: false

        // Checks for empty if blocks
        if (emptyThen || emptyElse) {
            checker.reportEmptyIfBlocks(ifStatement, emptyThen, emptyElse)
        }

        super.visitIfStatement(ifStatement, data)
    }
}