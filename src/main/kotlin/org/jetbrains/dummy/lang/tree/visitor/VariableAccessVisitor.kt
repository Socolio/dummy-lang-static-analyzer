package org.jetbrains.dummy.lang.tree.visitor

import org.jetbrains.dummy.lang.checker.VariableChecker
import org.jetbrains.dummy.lang.tree.*

/**
 * Visitor for finding illegal operations with variables.
 * Data : Map<VarName, DeclarationInfo>
 */
class VariableAccessVisitor(val checker: VariableChecker) :
    DummyLangVisitor<Unit, MutableMap<String, DeclaredVariable>>() {

    override fun visitElement(element: Element, data: MutableMap<String, DeclaredVariable>) {
        element.acceptChildren(this, data)
    }

    override fun visitFunctionDeclaration(
        functionDeclaration: FunctionDeclaration,
        data: MutableMap<String, DeclaredVariable>
    ) {
        data.clear()

        // Collect information about function parameters
        functionDeclaration.parameters.forEach { parameter ->
            if (data.containsKey(parameter)) {
                checker.reportDuplicatedFunctionParameters(functionDeclaration, parameter)
            } else {
                data[parameter] = DeclaredVariable(functionDeclaration.line, true)
            }
        }

        super.visitFunctionDeclaration(functionDeclaration, data)
    }

    override fun visitVariableDeclaration(
        variableDeclaration: VariableDeclaration,
        data: MutableMap<String, DeclaredVariable>
    ) {
        super.visitVariableDeclaration(variableDeclaration, data)
        val varName = variableDeclaration.name

        // Checks if function with this name already exists
        val declaredVar = data[varName]
        if (declaredVar != null) {
            checker.reportNameOverlapping(variableDeclaration, declaredVar.declarationLine)
        }

        // Collect info about variable (and replace existing if name overlapping)
        data[varName] = DeclaredVariable(variableDeclaration.line, variableDeclaration.initializer != null)
    }

    override fun visitIfStatement(ifStatement: IfStatement, data: MutableMap<String, DeclaredVariable>) {
        ifStatement.condition.accept(this, data)

        // Backup declaration data and visit 'then' block
        val thenData = data.mapValuesTo(HashMap(data.size)) { it.value.copy() }
        ifStatement.thenBlock.accept(this, thenData)

        // Visit 'else' block if exists
        if (ifStatement.elseBlock != null) {
            val elseData = data.mapValuesTo(HashMap(data.size)) { it.value.copy() }
            ifStatement.elseBlock.accept(this, elseData)

            // Merge information about variables from blocks
            data.forEach { (varName, declaredVar) ->
                val thenInitialized = thenData[varName]!!.initialized
                val elseInitialized = elseData[varName]!!.initialized

                declaredVar.initialized = declaredVar.initialized || thenInitialized && elseInitialized
                declaredVar.hasInitializationCases = !declaredVar.initialized && (thenInitialized || elseInitialized)
            }
        }
    }

    override fun visitAssignment(assignment: Assignment, data: MutableMap<String, DeclaredVariable>) {
        super.visitAssignment(assignment, data)

        val varName = assignment.variable
        val declaredVar = data[varName]

        // Checks for non existing variable
        if (declaredVar == null) {
            checker.reportAssignmentNonExistingVariable(assignment)
        } else {
            // Checks for self assignment
            if (assignment.rhs is VariableAccess && assignment.rhs.name == varName) {
                checker.reportAssignmentToItself(assignment)
            }

            // Update variable initialization info
            if (!declaredVar.initialized) {
                declaredVar.initialized = true
                declaredVar.hasInitializationCases = false
            }
        }
    }

    override fun visitVariableAccess(variableAccess: VariableAccess, data: MutableMap<String, DeclaredVariable>) {
        val varName = variableAccess.name
        val declaredVar = data[varName]

        // Checks for access to non existing or not initialized variable
        if (declaredVar == null) {
            checker.reportAccessToNonExistingVariable(variableAccess)
        } else if (!declaredVar.initialized) {
            checker.reportAccessBeforeInitialization(variableAccess, declaredVar.hasInitializationCases)
        }
    }
}

/**
 * Represents information about declared variable.
 * @param declarationLine  Line number with declaration.
 * @param initialized  If variable is initialized in all previous possible cases.
 * @param hasInitializationCases  If variable is not initialized at least in one of many possible cases.
 */
data class DeclaredVariable(
    val declarationLine: Int,
    var initialized: Boolean,
    var hasInitializationCases: Boolean = false
)
