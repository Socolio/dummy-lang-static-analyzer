package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.checker.AbstractChecker
import org.jetbrains.dummy.lang.checker.BlockChecker
import org.jetbrains.dummy.lang.checker.FunctionChecker
import org.jetbrains.dummy.lang.checker.VariableChecker
import org.jetbrains.dummy.lang.tree.DummySourceToTreeTransformer
import org.jetbrains.dummy.lang.util.DiagnosticReporter
import java.io.OutputStream

class DummyLanguageAnalyzer(outputStream: OutputStream, reportWarnings: Boolean = true) {

    companion object {

        private val CHECKERS: List<(DiagnosticReporter) -> AbstractChecker> = listOf(
            ::VariableChecker,
            ::FunctionChecker,
            ::BlockChecker
        )

    }

    private val reporter: DiagnosticReporter = DiagnosticReporter(outputStream, reportWarnings)

    fun analyze(path: String) {
        val transformer = DummySourceToTreeTransformer()
        val file = transformer.transform(path)

        val checkers = CHECKERS.map { it(reporter) }
        for (checker in checkers) {
            checker.inspect(file)
        }
    }
}

