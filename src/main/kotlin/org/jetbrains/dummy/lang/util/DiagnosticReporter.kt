package org.jetbrains.dummy.lang.util

import org.jetbrains.dummy.lang.tree.Element
import java.io.OutputStream
import java.io.PrintStream

class DiagnosticReporter(outputStream: OutputStream, val reportWarnings: Boolean) {

    private val outputStream = PrintStream(outputStream)

    fun error(element: Element, message: String) {
        outputStream.println("ERROR: line ${element.line}: $message")
    }

    fun warning(element: Element, message: String) {
        if (reportWarnings) {
            outputStream.println("WARNING: line ${element.line}: $message")
        }
    }
}