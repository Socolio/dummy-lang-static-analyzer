package org.jetbrains.dummy.lang.checker

import org.jetbrains.dummy.lang.tree.*
import org.jetbrains.dummy.lang.tree.visitor.VariableAccessVisitor
import org.jetbrains.dummy.lang.util.DiagnosticReporter

/**
 * Checks for illegal operations with variables.
 */
class VariableChecker(private val reporter: DiagnosticReporter) : AbstractChecker() {

    override fun inspect(file: File) {
        val visitor = VariableAccessVisitor(this)
        visitor.visitFile(file, mutableMapOf())
    }

    /**
     * Report access to variable that can not be fount in current scope.
     */
    fun reportAccessToNonExistingVariable(access: VariableAccess) {
        reporter.error(access, "Variable '${access.name}' not found")
    }

    /**
     * Report assignment of variable that can not be fount in current scope.
     */
    fun reportAssignmentNonExistingVariable(assignment: Assignment) {
        reporter.error(assignment, "Non existing variable '${assignment.variable}' can not be assigned")
    }

    /**
     * Report useless self assignment case.
     */
    fun reportAssignmentToItself(assignment: Assignment) {
        reporter.warning(assignment, "Variable '${assignment.variable}' is assigned to itself")
    }

    /**
     * Report access to variable that may be not initialized in some cases.
     * @param hasMultipleInitCases  If variable initialized at least in one case of many
     */
    fun reportAccessBeforeInitialization(access: VariableAccess, hasMultipleInitCases: Boolean) {
        if (hasMultipleInitCases) {
            reporter.error(access, "Variable '${access.name}' may not be initialized in some cases")
        } else {
            reporter.error(access, "Variable '${access.name}' is accessed before initialization")
        }
    }

    /**
     * Report variable declaration in scape that already contains variable with similar name.
     * @param declarationLine  Line with original variable declaration.
     */
    fun reportNameOverlapping(declaration: VariableDeclaration, declarationLine: Int) {
        reporter.warning(
            declaration,
            "Variable '${declaration.name}' overlaps already declared variable at line $declarationLine"
        )
    }

    /**
     * Report function with duplicated parameter names.
     * @param parameter  Name of parameter that exists in function many times.
     */
    fun reportDuplicatedFunctionParameters(function: FunctionDeclaration, parameter: String) {
        reporter.error(function, "Function '${function.name}' has more than one '$parameter' parameter")
    }
}