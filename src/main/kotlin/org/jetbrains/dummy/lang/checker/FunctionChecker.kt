package org.jetbrains.dummy.lang.checker

import org.jetbrains.dummy.lang.tree.*
import org.jetbrains.dummy.lang.tree.visitor.function.DeclaredFunction
import org.jetbrains.dummy.lang.tree.visitor.function.FunctionCallVisitor
import org.jetbrains.dummy.lang.tree.visitor.function.FunctionDeclarationVisitor
import org.jetbrains.dummy.lang.tree.visitor.function.FunctionInterruptionVisitor
import org.jetbrains.dummy.lang.util.DiagnosticReporter

/**
 * Checks for illegal operations with functions.
 */
class FunctionChecker(private val reporter: DiagnosticReporter) : AbstractChecker() {

    override fun inspect(file: File) {
        // Map<Name, <Map<ParamsCount, Function>>
        val declarationData = mutableMapOf<String, MutableMap<Int, DeclaredFunction>>()

        // Collects declaration data
        val declarationVisitor = FunctionDeclarationVisitor(this)
        declarationVisitor.visitFile(file, declarationData)

        // Collects data about functions return types
        val interruptionVisitor = FunctionInterruptionVisitor(this)
        interruptionVisitor.visitFile(file, declarationData)

        // Checks for illegal operations
        val callVisitor = FunctionCallVisitor(this)
        callVisitor.visitFile(file, declarationData)
    }

    /**
     * Report function that is already declared in file.
     * @param parametersCount  Amount of function arguments.
     * @param declarationLine  Line with duplicated function declaration.
     */
    fun reportDuplicatedDeclaration(declaration: FunctionDeclaration, parametersCount: Int, declarationLine: Int) {
        reporter.error(
            declaration, when (parametersCount) {
                0 -> "Function '${declaration.name}' without parameters already declared at line $declarationLine"
                1 -> "Function '${declaration.name}' with one parameter already declared at line $declarationLine"
                else -> "Function '${declaration.name}' with $parametersCount parameters already declared at line $declarationLine"
            }
        )
    }

    /**
     * Report invocation of function that can not be found with any amount of parameters.
     */
    fun reportCallNonExistingFunction(call: FunctionCall) {
        reporter.error(call, "Function '${call.function}' not found")
    }

    /**
     * Report invocation of function that can not be found with specific amount of parameters.
     * @param parametersCount  Amount of parameters in function invocation.
     */
    fun reportCallWithDifferentParametersCount(call: FunctionCall, parametersCount: Int) {
        reporter.error(
            call, when (parametersCount) {
                0 -> "Function '${call.function}' without parameters not found"
                1 -> "Function '${call.function}' with one parameter not found"
                else -> "Function '${call.function}' with $parametersCount parameters not found"
            }
        )
    }

    /**
     * Report return statement which incompatible with any previous possible return statement in function body.
     */
    fun reportIncompatibleWithPreviousReturnType(returnStatement: ReturnStatement) {
        reporter.error(returnStatement, "Return statement is incompatible with previous returning cases")
    }

    /**
     * Report return statement which incompatible with return statement in other if statement case.
     */
    fun reportIncompatibleCasesReturnType(ifStatement: IfStatement) {
        reporter.error(ifStatement, "If statement has incompatible return types in its cases")
    }

    /**
     * Report lines of code which will never be called.
     * @param endStatement  Last line of unreachable code.
     */
    fun reportUnreachableCode(startStatement: Statement, endStatement: Statement) {
        if (startStatement === endStatement) {
            reporter.warning(startStatement, "Unreachable code")
        } else {
            reporter.warning(startStatement, "Unreachable code at lines ${startStatement.line}-${endStatement.line}")
        }
    }

    /**
     * Report function with possible return cases but without final return statement.
     */
    fun reportIncompleteReturnCases(function: FunctionDeclaration) {
        reporter.error(function, "Function '${function.name}' has incomplete return cases")
    }

    /**
     * Report variable assign by invocation of void function.
     * @param varName  Name of assigned variable.
     * @param call  Statement with void function invocation.
     * @param declaredFunction  Info about void function.
     */
    fun reportAssignmentByVoidFunction(
        statement: Statement,
        varName: String,
        call: FunctionCall,
        declaredFunction: DeclaredFunction
    ) {
        reporter.error(
            statement,
            "Variable '$varName' cat not be assigned by void function '${call.function}' that is declared at line ${declaredFunction.line}"
        )
    }
}