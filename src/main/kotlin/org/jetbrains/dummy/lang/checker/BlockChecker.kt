package org.jetbrains.dummy.lang.checker

import org.jetbrains.dummy.lang.tree.BooleanConst
import org.jetbrains.dummy.lang.tree.File
import org.jetbrains.dummy.lang.tree.FunctionDeclaration
import org.jetbrains.dummy.lang.tree.IfStatement
import org.jetbrains.dummy.lang.tree.visitor.BlockVisitor
import org.jetbrains.dummy.lang.util.DiagnosticReporter

/**
 * Checks for useless or empty blocks of code.
 */
class BlockChecker(private val reporter: DiagnosticReporter) : AbstractChecker() {

    override fun inspect(file: File) {
        val visitor = BlockVisitor(this)
        visitor.visitFile(file, Unit)
    }

    /**
     * Report function with empty or single return line body.
     */
    fun reportEmptyFunction(function: FunctionDeclaration) {
        reporter.warning(function, "Function '${function.name}' is empty")
    }

    /**
     * Report if statement with at least one empty case.
     * @param emptyThen  If 'then' block is empty.
     * @param emptyElse  If 'else' block is empty.
     */
    fun reportEmptyIfBlocks(ifStatement: IfStatement, emptyThen: Boolean, emptyElse: Boolean) {
        when {
            emptyThen && emptyElse -> reporter.warning(ifStatement, "Useless if statement")
            emptyThen -> reporter.warning(ifStatement, "If statement has empty then block")
            emptyElse -> reporter.warning(ifStatement, "If statement has empty else block")
        }
    }

    /**
     * Report if statement with constant Boolean in condition.
     */
    fun reportConstantIfCondition(ifStatement: IfStatement, condition: BooleanConst) {
        reporter.warning(ifStatement, "If statement has constant '${condition.value}' in condition")
    }

}